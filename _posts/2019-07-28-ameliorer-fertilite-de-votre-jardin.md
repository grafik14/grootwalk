---
layout: post
title:  "Améliorer la fertilité de votre jardin"
---

Une bonne terre est essentielle pour avoir un potager productif. Voici quelques
trucs pour améliorer la fertilité de la terre.


# Le paillage

La terre laissée nue détruit les micros-organismes nécessaires à sa
fertilisation. Il faut donc la couvrir d'un paillage suffisant de 20 cms de foin
(herbe coupée). Les avantages de cette méthode sont les suivants : 

- réguler la température de la terre en hiver et en plein été
- garder l'humidité au niveau du sol et réduire les arrosages
- protéger les fruits afin qu'ils ne soient pas en contact direct avec la terre
- limiter le développement des aventices (mauvaises herbes)
- favoriser le développement des micro-organismes nécessaires à la fertilisation
    et au travail de la terre


# Le BRF

Le BRF (Broyat Rameal Fragmenté) est un mélange de résidus de broyage de rameaux
de bois d'arbres feuillus. 

Le rôle du BRF est :

- nutritif : rôle prépondérant de la jeune lignine, favorisant la conservation
    et la distribution d'eau par symbiose entre les hyphes mycéliens.
- irrigateur et ameublissement du sol : limite le lessivage du sol par la pluie
    et l'évaporation pendant les chaleurs.
- thermorégulateur : isole le sol et limite les transferts de température de
    l'air ambiant vers la terre.

Le BRF est très adapté pour les plante des sous-bois : fraise, framboise,
cassis, mure, groseille


# Les engrais verts

Après la récolte, à la fin de l'été, plantez de l'engrais vert pour occuper le
terrain, éviter que les mauvaises herbes prolifèrent, ameublir la
terre et l'enrichir à l'azote.

Parmi les engrais verts, on distingue les plantes qui ont un système racinaire
important et les plantes issues de la famille des Fabacées (anciennement
légumineuses) qui accumulent de l'azote.

## Engrais vert avec système racinaire important

- seigle : résistante au froid, étouffe le liseron, à associer avec la vesce
- moutarde : croissance rapide, capte l'azote, résiste au froid, et désinfecte
    le sol, mellifère
- phacélie : très bon désherbant, mellifère, engrais vert très complet


## Engrais vert de la famille des Fabacées

- vesce : plante annuelle (famille des légumineuses), terre argileuse, 
  apporte de l'azote, étouffe les mauvaises herbes, à associer avec le seigle.
- fève : rustique, terrain lourd


# Elever des poules

Une autre possiblité pour améliorer la structure du sol est d'élever des poules.

- Les poules grattent en permanence le sol pour chercher leur nourriture. 
- Elles se nourrissent également de mauvaises herbes. 
- Et pour finir, la fiente de poule est un excellent engrais pour la terre. 


