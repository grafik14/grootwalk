---
layout: post
title:  "Les catégories de légumes"
---

Plusieurs classifications existent pour les légumes. Il est important de les
connaitre pour concevoir l'aménagement du jardin et pour faire évoluer au mieux
le jardin.

# Type de légume

- légume-racine : betterave, carotte, navet, radis
- légume-fruit : aubergine, concombre, courgette, tomate
- légume-feuille : choux, épinard, poireau, salade
- légume-grain ou légumineuse : haricot, pois

# Famille botanique

- Liliacée : ail, oignon, échalote, poireau
- Solanacée : tomate, pomme de terre, aubergine
- fabacée ; pois, haricot
- brassicacée : chou, radis, navet
- cucurbitacée : courgette, concombre, potiron

# En fonction des besoin 

- légumes avec de gros besoins : tomates, courges, épinards, choux
- besoins normaux : carotte, céleri, salade
- besoins faibles : pois, haricot, oignon

# Aménagement

- Légumes du soleil : tomate, aubergine, piment/poivron, melon
- légumes lents : courge, courgette, potiron, bette, carotte, poireau,
  céleri, fenouil, choux, concombre, betterave, fève, maïs
  ail, échalote, oignon
- légumes rapides : radis, navet, haricot nain, épinard, choux-rave, laitue,
    oignon blanc, pois, 

