---
layout: post
title:  "La taille des curcubitacées : courges, potiron et potimarron"
---

Les courges, potirons et potimarrons sont des plantes coureuses qui nécessitent d'être taillées pour produire rapidement. De plus les fleurs de ces légumes peuvent être mâle ou femelle et seule les femelles peuvent produire des fruits, contrairement aux tomates ou poivrons. 

La distinction mâle/femelle se fait de la manière suivante : 

- les fleurs mâles apparaissent avant les femelles et sont portées par un long pédoncule.
- les fleurs femelles sont situées sur une petite boule qui sera à l'origine du futur légume.


## Pollinisation des fleurs femelles

La pollinisation des fleurs femelles par les fleurs mâles peut se faire par les insectes polénisateurs. Si il n'y en a pas, il faudra ajouter des planter méllifères. A défaut, la pollinisation devra se faire manuellement d'une fleur mâle vers une fleur femelle.

Il faut éviter que la fécondation croisée ne se fasse entre différentes plantes (potimarron avec potiron par exemple) car cela la descendance peut perdre la propriétés culinaire ou esthétique des parents. Pour cela, il faut favoriser la fécondation des fleurs mâles avec les fleurs femelles d'une même plante et séparer les différentes plantations : courges, potiron, potimarron.

## Taille des curcubitacées coureuses

La taille des curcubitacées se déroule en 3 phases : 

1. Lorsque la tige principale (fleurs mâles) compte 4 à 5 feuilles, il faut couper la tige après la deuxième feuille afin de favoriser le développement des rameaux latéraux.

2. Les rameaux latéraux doivent être taillés quand ils ont 8 à 10 feuilles. La taille s'effectue après la 5ème feuille. Ils n'ont pas de fruit.

3. Lorsque des rameaux se développent avec des fruits, il faut laisser 2 feuilles après le fruit et tailler le rameau après.

Pour un seul pied, il faut compter 3 fruits pour avoir des gros potirons, 6 fruits pour avoir des potirons de taille moyenne.

