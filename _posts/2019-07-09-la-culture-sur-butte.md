---
layout: post
title:  "La culture sur butte"
---

# La permaculture ou la culture permanente

Les buttes font 1,20m de large, 50 cm de haut et sont espacées de 50 cms pour
permettre le passage.

Les buttes classiques associent grosses branches, broyats forestiers, compost
terre et paillage. Elles sont bordées par des planches d'une vingtaine de cms
pour éviter l'affaissement de la butte.

## Mise en place d'une butte "Hugelkultur"

1. Aérer la terre avec une grelinette / aéro-fourche spéciale permaculture
2. Dégager les mauvaises herbes, gazon et mottes de terre qui vont avec.
3. Récupérer de la terre fine (si possible) en-dessous et la mettre de côté sur
   une bache
4. Disposer une couche de rondins, une couche de branchage, ensuite les mottes de
   terre avec gason, du compost, la terre fine mise de côté et du paillage.

Sources : https://fermesdavenir.org/

Le plus : Afin de favoriser le développement des lombrics, installer au milieu
de la butte un gros pot en terre cuite avec un trou au fond. Mettre du compost
au fond, puis 2 poignées de lombrics, puis des déchets alimentaires pour nourrir
les lombrics. Attention, les lombrics ne supportent pas la lumière du soleil.

## Buttes circulaire et spirales

Les buttes "en trou de serrure" sont circulaires et ont un renfoncement pour
pouvoir accéder au centre où se trouve un composteur qui permet de déposer des
déchets alimentaires à composter. Ces buttes peuvent être utiliser pour des
plantes aromatiques. Elles sont en générale délimitées par des petits murets
d'une hauteur de 40 cms fait en agglo ou briques non maçonnées.

Il existe aussi des buttes en spirale avec toujours au centre le composteur.

## Buttes en lasagne

Le terrain est recouvert de cartons, puis différentes couches sur 60 cms : tontes de gazon,
feuilles morte, branchages, paille, compost grossier, compost mur et
terre de jardin. 

La butte va ensuite se tasser pour mesurer encore 30 cms.

## Autre préparation d'une planche de culture

Si la planche est envahie par les mauvaises herbes :

- aérer la terre avec une biogriffe
- arracher les mauvaises herbes
- rateler
- faire un paillage avec des herbes fauchées
- recouvrir des mauvaises herbes arrachées

[https://fr.wikipedia.org/wiki/Culture_sur_butte](https://fr.wikipedia.org/wiki/Culture_sur_butte)


