---
layout: post
title:  "Les associations au jardin"
---

Les associations au jardin cherchent à cultiver des légumes complémentaires
ensemble. Par exemple, on parle souvent d'associer le poireau et la fraise,
ainsi que la carotte et l'oignon.

L'objectif des associations de légumes peut être de lutter contre les nuisibles,
d'éviter les maladies ou de favoriser la croissance des légumes.

# Les associations bénéfiques

- technique des trois sœurs : le maïs, le haricot grimpant et les courges
- la carotte et l'oignon / l'échalotte / l'ail ou le poireau
- le chou et le céléri
- le poireau et le fraisier
- la pomme de terre et l'ail
- les oeillets d'inde ou le basilic protègent la tomate
- laitue protège le radis / navets ou choux-raves
- le cerfeuil protège la salade 
- la sariette protège les haricots
- l'ail protège les fraisiers

# Les associations à éviter 

- pois, haricots et fèves ne supportent pas les oignons, échalotes, ails et
    poireaux
- tomates et courgettes n'aiment pas les concombres
- betteraves et bettes n'aiment pas les épinards
- salades n'aiment pas le persil et le céleri

# Les fruitiers

## Besoin d'azote

Les fruitiers de la même espèce doivent être éloignés et séparés par des
fixateurs d'azote. Les fixateurs d'azote peuvent être des légumineuses :

- Arbres : févier d'amérique, robinier faux acacia, sophora japonica, aragana
    arborescens
- Arbustes : genêts (cytises, ajoncs, genêts vrais) non comestibles, mais très
    bon fournisseur d'azote et très bon mellifères.
- Plantes vivaces : lupin vivace et lupin blanc, luzerne (très vigoureuse),
    trèfle. La luzerne est utilisée avec le trèfle. La vesces et le seigle sont
    également complémentaires.
- Plantes annuelles : haricots nains et à rames, pois, fèves, lentilles

## Besoin des poules

Les poules dans les espaces enherbés à proximité des fruitiers se nourrissent
des fruits tombés à terre et des vers et limitent ainsi le développement des
maladies et parasites des arbres fruitiers.

De plus, les poules enrichissent le sol avec leurs fientes qui constituent un
engrais

# Liens : 

Exemple de compagnonnage : [https://fr.wikipedia.org/wiki/Culture_associ%C3%A9e#Exemples_de_compagnonnage](https://fr.wikipedia.org/wiki/Culture_associ%C3%A9e#Exemples_de_compagnonnage)



